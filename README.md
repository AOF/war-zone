# README #

Generate historical missile launch trajectories using google earth .KML file - you just have to redefine the initial variables

### What is this repository for? ###

* Historical Missile trajectory recreation

### How do I get set up? ###

* You need the missile type / the launch location / the target location
* get Processing: 
  -> http://www.processing.org/
* get Google Earth: 
  -> https://www.google.com/earth/

### Development ###

* Processing & Ballistics development : Ivan Murit
  -> http://ivan-murit.fr
* project and other aspects: Nicolas Maigret
  -> http://peripheriques.free.fr/blog/
