PrintWriter out;
float[] o = { 0,17.76,   3.7,17.706,   0,14.8,  
              0,7.64,  0.116,12.37,   6.44,19.936,  
              20.063,22.047,   50.286,59.937,   78.73,78.381, 
              136.764,117.464,   457.282,7.64,   457.282,117  };
float[]oo={ o[6], o[7],
            o[6],  o[7],  o[8],  o[9]-1,  o[8],  o[9]-1 ,   
            o[4]+0.22,  o[5]+0.5,  o[2],  o[3]-0.23 ,  o[10], o[11],
            o[10], o[11], o[14], o[15], o[16], o[17] ,
            o[18], o[19]-1, o[22], o[23], o[20], o[21] };
//double latDebut = 52.1005805, lonDebut = 4.2894822, latFin = 51.566914, lonFin = -0.048811;int heading=260,v=1;float t=0.03; // V2 la Haye / londres
double latDebut = 30.846822, lonDebut = 17.882958, latFin = 30.4181797, lonFin = 19.6555354;int heading=80, v=2;float t=0.005; // V2 SYRIE - el Breda

double latDist = latFin-latDebut, lonDist = lonFin-lonDebut;
float dirOld=180;
void setup() {
  size(1000,500); background(0); strokeWeight(0.2); scale(2); smooth(8);
  out = createWriter("map.kml");  
  noFill();stroke(255);  
  beginShape();
    vertex(oo[0], oo[1]);
    for (int i = 0; i<4; i++){
      int j = i*6;
      bezierVertex( oo[2+j], oo[3+j], oo[4+j], oo[5+j], oo[6+j], oo[7+j] );}
  endShape();
  out.println( "<?xml version='1.0' encoding='UTF-8'?>  <kml xmlns='http://www.opengis.net/kml/2.2'  xmlns:gx='http://www.google.com/kml/ext/2.2'><Document>  <open>1</open><gx:Tour> <name>V2</name><gx:Playlist>");

  flyTo(latDebut,lonDebut,0,0,90,0,0);
  paused(5);
  flyTo(latDebut,lonDebut,0,3,90,0,0);
  flyTo(latDebut,lonDebut,0,2,180,0,0);
  paused(3);
  courbe(v*400, 0,t);
  courbe(v*500, 1,t);
  courbe(v*2000,2,t);
  courbe(v*7000,3,t);
  paused(5);
  
  String fin = "</gx:Playlist> </gx:Tour> </Document> </kml>";
  out.println(fin); 
  out.flush();
  out.close();
  exit();
}
void courbe( int steps,int id, float speed ){
  id = id*6;
  for (int i = 0; i <= steps; i++) {
    float t = i / float(steps);
    float x = bezierPoint(oo[0+id],oo[2+id],oo[4+id],oo[6+id], t); float y = bezierPoint(oo[1+id],oo[3+id],oo[5+id],oo[7+id], t); 
    float tx = bezierTangent(oo[0+id],oo[2+id],oo[4+id],oo[6+id], t); float ty = bezierTangent(oo[1+id],oo[3+id],oo[5+id],oo[7+id], t);
    float dir = 90+degrees(atan2(ty, tx));
    if( i==0||i==steps ) dir = dirOld ; dirOld = dir ; // RECTIF direction paliers

    float vibs1=0, vibs2=0, vibs3=0;   // VIBRATIONS 
    float[] start = {0.2, 0.2, 0.2, 0.2}; 
    float[] end   = {0.2, 0.2, 0.2, 0.1}; 
    
    for (int j = 0; j<4; j++){
      float puissance = map(i,0,steps,start[j],end[j]);
    
      if(id==0) puissance = map(i,0,steps/1.5, 0.7, 0.2);  // lancement
      vibs1 = random( -puissance, puissance );
      vibs3 = random( -puissance, puissance );
      if(i>2000 && id==18 ) vibs2 = random( -puissance/30, puissance/30 );  //  roll
      if( id==0 )   vibs2 = random( -puissance/10, puissance/10 );  //  roll
    }

    flyTo(latDebut+x/o[20]*latDist, 
          lonDebut+x/o[20]*lonDist, 
          (y-7.58)/80.754*100000, speed, dir+vibs1, vibs2, vibs3);
  }
}

void flyTo(double lat,double lon,double alt,float dur,float dir, float rol, float ori) {
  out.println("<gx:FlyTo>"+
    "<gx:duration>"+dur+"</gx:duration>"+
    "<gx:flyToMode>smooth</gx:flyToMode>"+
      "<Camera>"+ 
        "<gx:horizFov>"+100+"</gx:horizFov>"+// ouverture de la camera
        "<gx:TimeStamp>"+
          "<when>T17:00:00-17:00</when>"+
        "</gx:TimeStamp>"+
        "<latitude>"+ lat +"</latitude>"+
        "<longitude>"+ lon +"</longitude>"+
        "<altitude>"+ alt +"</altitude>"+ //sqrt(5000-i*i)+
        "<heading>"+(heading+ori)+"</heading>"+  // topview + rotation g-d
        "<tilt>"+dir+"</tilt>"+  // floor to horizon
        "<roll>"+rol+"</roll>"+ 
        "<altitudeMode>relativeToSeaFloor</altitudeMode>"+ 
      "</Camera>"+
    "</gx:FlyTo>"); 

  println(" lat: "+lat+" lon: "+lon+" alt: "+alt+" dur: "+dur+" dir: "+dir);
}
void paused(float dur){ out.println("<gx:Wait><gx:duration>"+dur+"</gx:duration></gx:Wait>"); }
void draw(){  frame.setTitle(mouseX+" x "+mouseY);  }