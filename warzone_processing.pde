
//    historical missile launch trajectories
//    this script generate google earth KML file
//    you just have to redefine the following variables

//        START LATITUDE : latStart
//        START LONGITUDE : lonStart
//        END LATITUDE : latEnd
//        END LONGITUDE : lonEnd
//        DIRECTION (NORTH to NORTH = 0° to 360°) : heading 
//        ACCURATIE OF THE CURVES : steps
//        TIME BETWEEN STEPS : speed 
//        TRAJECTORY : weapon : 1 = BALISTIC, 2 = AIR TO SURFACE

// MISSILE SCUD - Syrie > el Breda 
double latStart = 30.846822, lonStart = 17.882958, latEnd = 30.4181797, lonEnd = 19.6555354;int heading=80, steps=2, weapon=1;float speed=0.005*8;

// MISSILE V2 - The Hague > London 
//double latStart = 52.1005805, lonStart = 4.2894822, latEnd = 51.566914, lonEnd = -0.048811;int heading=260,steps=1, weapon=1; float speed=0.03;

// air to surface strike Israel > Gaza strip 
//double latStart = 31.5493786, lonStart = 34.5655821, latEnd = 31.5335242, lonEnd = 34.5063105; int heading=270, steps=1, weapon=2; float speed=0.001*8;

// SCUD golf war
//double latStart = 29.2945994, lonStart = 47.6707101, latEnd = 27.0234989, lonEnd = 49.6870148; int heading=130, steps=2, weapon=1; float speed=0.005*8;

//------------------------------------------------------

// SHAPE OF A BALISTIC MISSILE'S FLIGHT IN 4 CURVES (BEZIER CURVES FROM SVG WHICH DEFINE THE ALTITUDE)
float[] o = { 0,17.76,   3.7,17.706,   0,14.8,  0,7.64,  0.116,12.37,   6.44,19.936,  20.063,22.047,   50.286,59.937,   78.73,78.381,    136.764,117.464,   457.282,7.64,   457.282,117  };
float[]oo = { o[6], o[7], o[6], o[7], o[8], o[9]-1, o[8], o[9]-1 , o[4]+0.22, o[5]+0.5, o[2], o[3]-0.23, o[10], o[11], o[10], o[11], o[14], o[15], o[16], o[17], o[18], o[19]-1, o[22], o[23], o[20], o[21] };
double latDist = latEnd-latStart, lonDist = lonEnd-lonStart;
float dirOld=180;
PrintWriter out;

void setup() {
  out = createWriter("map.kml");  
  out.println( "<?xml version='1.0' encoding='UTF-8'?>  <kml xmlns='http://www.opengis.net/kml/2.2'  xmlns:gx='http://www.google.com/kml/ext/2.2'><Document>  <open>1</open><gx:Tour> <name>V2</name><gx:Playlist>");

// MISSILE FLYING PLAN 
  switch(weapon){
    case 1 : // balistic
      flyTo(latStart,lonStart,0,0,90,0,0);
      paused(6);
      flyTo(latStart,lonStart,0,2*8,180,0,0);
      paused(3);
      courbe(steps*400, 0,speed, 100000, 20);
      courbe(steps*500, 1,speed, 100000, 20);
      courbe(steps*2000,2,speed, 100000, 20);
      courbe(steps*7000,3,speed, 100000, 20);
      break;
    case 2 : // air to surface
      flyTo(latStart-latDist*3,lonStart-lonDist*3,900,0,110,0,0);
      flyTo(latStart,lonStart,900,15*8,110,0,0);
      paused(0.001);
      courbe(steps*7000,3,speed, 900, 20);
      //flyTo(latEnd,lonEnd,20,5,0,0,0);
      break; 
  }

  paused(5*8);

  out.println("</gx:Playlist> </gx:Tour> </Document> </kml>"); 
  out.flush();
  out.close();
  exit();
}
void courbe( int steps, int id, float speed, int altitude, int vibration){

  // CALCULATE EACH STEP OF A CURVE
  id = id*6;
  for (int i = 0; i <= steps; i++) {  
    float t = i / float(steps);
    float x = bezierPoint(oo[0+id],oo[2+id],oo[4+id],oo[6+id], t); float y = bezierPoint(oo[1+id],oo[3+id],oo[5+id],oo[7+id], t); 
    float tx = bezierTangent(oo[0+id],oo[2+id],oo[4+id],oo[6+id], t); float ty = bezierTangent(oo[1+id],oo[3+id],oo[5+id],oo[7+id], t);
    float dir = 90+degrees(atan2(ty, tx));
    if( i==0||i==steps ) dir = dirOld ; dirOld = dir ;

    // VIBRATIONS
    float vibs1 = random( -0.01*vibration, 0.01*vibration ), // DIRECTION
      //vibs2 = random( -0.02, 0.02 ), // ROLL
      vibs3 = random( -0.01*vibration, 0.01*vibration ); // HEADING

    flyTo(latStart+x/o[20]*latDist, 
          lonStart+x/o[20]*lonDist, 
          (y-7.58)/80.754*altitude, speed, dir+vibs1, 0, vibs3);
  }
}
// TRANSCRIPTION IN KML LANGUAGE FOR GOOGLE EARTH >>> https://developers.google.com/kml/documentation/kmlreference
void flyTo(double lat,double lon,double alt,float dur,float dir, float rol, float ori) {
  out.println("<gx:FlyTo>"+
    "<gx:duration>"+dur+"</gx:duration>"+ // TIME TO DO THE STEP
    "<gx:flyToMode>smooth</gx:flyToMode>"+
      "<Camera>"+ 
        "<gx:horizFov>"+100+"</gx:horizFov>"+// CAMERA LENS APERTURE
//        "<gx:TimeStamp><when>T15:00:00</when></gx:TimeStamp>"+
        "<latitude>"+ lat +"</latitude>"+
        "<longitude>"+ lon +"</longitude>"+
        "<altitude>"+ alt+200 +"</altitude>"+ 
        "<heading>"+(heading+ori)+"</heading>"+  // DIRECTION LEFT-RIGHT
        "<tilt>"+dir+"</tilt>"+  // DIRECTION FLOOR TO HORIZON
        "<roll>"+rol+"</roll>"+
        "<altitudeMode>relativeToGround</altitudeMode>"+ 
      "</Camera>"+
    "</gx:FlyTo>"); 
//  println(" lat: "+lat+" lon: "+lon+" alt: "+alt+" dur: "+dur+" dir: "+dir);
}

void paused(float dur){ out.println("<gx:Wait><gx:duration>"+dur+"</gx:duration></gx:Wait>"); }
